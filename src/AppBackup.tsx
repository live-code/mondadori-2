import React, { useState } from 'react';
import Demo1 from './pages/Demo1';
import Demo2 from './pages/Demo2';
import Demo3 from './pages/Demo3';
import { Demo4 } from './pages/Demo4';
import { Demo5 } from './pages/demo5/Demo5';
import { Demo6 } from './pages/Demo6';
import { Demo7 } from './pages/demo7/Demo7';

const pages = [
  { path: 'demo1', label: '#1', component: <Demo1/>},
  { path: 'demo2', label: '#2', component: <Demo2/>},
  { path: 'demo3', label: '#3', component: <Demo3/>},
  { path: 'demo4', label: '#4', component: <Demo4/>},
  { path: 'demo5', label: '#5 CRUD', component: <Demo5/>},
  { path: 'demo6', label: '#6 (tooltip)', component: <Demo6/>},
  { path: 'demo7', label: '#7 (components)', component: <Demo7/>},
  { path: 'demo8', label: '#8 (components)', component: <Demo7/>},
]
function App() {
  const [page, setPage] = useState<string>('demo1')

  return (
    <div>
      <nav>
        {
          pages.map(p => {
            return <button key={p.path} onClick={() => setPage(p.path)}>{p.label}</button>
          })
        }
      </nav>
      {page}

      <hr/>
      {
        pages.map(p => {
          return p.path === page && <div key={p.path}> {p.component}</div>;
        })
      }
    </div>
  );
}

export default App;

