import clsx from 'clsx';
import React from 'react';
import { Link, NavLink } from 'react-router-dom';

interface isActiveProps {
  isActive: boolean,
  isPending: boolean
}

const itemIsActive = (obj: isActiveProps ) => {
  return clsx('btn', { 'btn-primary': obj.isActive })
}

export function NavBar() {
  return <div>
    <NavLink to="login" className={itemIsActive}>Login</NavLink>
    <NavLink to="demo1" className={itemIsActive}>Demo1</NavLink>
    <NavLink to="demo2" className={itemIsActive}>Demo2</NavLink>
    <NavLink to="demo3" className={itemIsActive}>Demo3</NavLink>
    <NavLink to="demo4" className={itemIsActive}>Demo4</NavLink>
    <NavLink to="demo5" className={itemIsActive}>Demo5</NavLink>
    <NavLink to="demo6" className={itemIsActive}>Demo6</NavLink>
    <NavLink to="demo7" className={itemIsActive}>Demo7</NavLink>
    <NavLink to="demo8" className={itemIsActive}>Demo8</NavLink>
    <NavLink to="demo9" className={itemIsActive}>Demo9</NavLink>
    <NavLink to="users" className={itemIsActive}>Users</NavLink>

  </div>
}
