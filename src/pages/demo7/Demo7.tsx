import axios from 'axios';
import { useEffect, useState } from 'react';
import { Landing } from './model/landing';
import { Spinner } from '../../uikit/Spinner';
import { Demo7Layout } from './components/Demo7Layout';

export function Demo7() {
  const [landing, setLanding] = useState<Landing | null>(null)

  useEffect(() => {
    axios.get<Landing>('http://localhost:3001/landing')
      .then(res => {
        setLanding(res.data)
      })
  }, [])

  return landing ?
    <Demo7Layout landing={landing} title="my landing page" /> :
    <Spinner />
}



