
export interface Hero {
  image: string;
  title: string;
}

export interface News {
  id: number;
  title: string;
}

export interface Profile {
  name: string;
  desc: string;
}

export interface Landing {
  hero: Hero | null;
  news: News[] | null;
  profile: Profile;
}
