import { News } from '../model/landing';

interface LandingNewsListProps {
  items: News[];
  onItemClick: (item: News) => void;
}

export function LandingNewsList (props: LandingNewsListProps) {
  return (
    <ul className="list-group">
      {
        props.items.map((item) => {
          return <li className="list-group-item"
                     key={item.id}
                     onClick={() => props.onItemClick(item)}
          >{item.title} </li>
        })
      }
    </ul>
  )
}

