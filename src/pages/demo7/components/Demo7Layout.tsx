import { Card } from '../../../uikit/Card';
import { Landing, News } from '../model/landing';
import { Hero } from './Hero';
import { LandingNewsList } from './LandingNewsList';

interface Demo7LayoutProps {
  landing: Landing
  title: string;
}

export function Demo7Layout(props: Demo7LayoutProps) {
  const { landing } = props;

  function gotoGoogle() {
    window.open('http://www.google.com')
  }

  function doSomethingElse() {
    console.log(' do something else')
  }

  function openWikipedia(text: string) {
    window.open(`https://it.wikipedia.org/wiki/${text}`)
  }

  return (
    <div>
      {landing?.hero && <Hero hero={landing.hero} /> }

      <div className="row">
        <div className="col-6">
          <LandingNewsList
            items={landing.news || []}
            onItemClick={news => openWikipedia(news.title)}
          />
        </div>


        <div className="col-6">
          <Card
            title={landing?.profile.name}
            type="success"
            icon="fa fa-facebook"
            onIconClick={doSomethingElse}
          > {landing?.profile.desc} </Card>
        </div>
      </div>
    </div>
  )
}
