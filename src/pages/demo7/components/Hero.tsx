import { Card } from '../../../uikit/Card';
import { Hero as HeroModel } from '../model/landing';

interface HeroProps {
  hero: HeroModel;
}

export  function Hero (props: HeroProps) {
  return <Card title={props.hero.title}>
    <img width="100%" src={props.hero.image} alt="hero image"/>
  </Card>
}

