import { Link, Outlet } from 'react-router-dom';

export function Login() {
  return <div>
    <h1>Welcome to our App</h1>

    <hr/>

    <Outlet />

    <hr/>
    <Link to="signin">SignIn</Link>
    <Link to="/login/lostpass">Lost Pass</Link>
    <Link to="registration">Registration</Link>
  </div>
}
