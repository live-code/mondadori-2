import { useEffect, useState } from 'react';

export function Demo4() {
  const [counter, setCounter] = useState<number>(0); // HOOK+
  const [value, setValue] = useState<number>(Math.random()); // HOOK+

  useEffect(() => {
    console.log('init')
  }, [])

  useEffect(() => {
    console.log('useeffect', counter)
  }, [counter])

  function inc() {
    setCounter(s => s + 1)
  }

  return <div>
    <h1>Demo Counter: {counter} - {value}</h1>
    <button onClick={() => setValue(Math.random())}>random</button>
    <button onClick={inc}>+</button>
  </div>
}
