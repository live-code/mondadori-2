import { useState } from 'react';

export default function Demo3() {
  const [yourname, setYourname] = useState<string>('Guest')
  const [counter, setCounter] = useState<number>(0)

  function updateCounter() {
    setCounter(s => s + 1)
    // setCounter(counter + 1)
  }

  return <div>
    <h1>Demo {counter}</h1>
    {yourname} {counter}
    <button onClick={updateCounter}>+</button>
    <button onClick={() => setYourname('pippo')}>CHANGE NAME</button>
  </div>
}
