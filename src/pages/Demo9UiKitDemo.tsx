import { useState } from 'react';
import Button from '../uikit/Button';
import ButtonGroup from '../uikit/ButtonGroup';
import { Card } from '../uikit/Card';
import { Divider } from '../uikit/Divider';
import Leaflet from '../uikit/Leaflet';
import MapQuest from '../uikit/MapQuest';
import { Spinner } from '../uikit/Spinner';

export default function Demo9UiKitDemo() {
  const [coords, setCoords] = useState<[number, number]>([42, 12]);
  const [zoom, setZoom] = useState<number>(7);

  function doSomething() {
    console.log('ciao')
  }

  return <div>
    <h1>UIKIT</h1>

    <div className="row">
      <div className="col"></div>
    </div>


    <Leaflet coords={coords} zoom={zoom}/>
    <Button onClick={() => setZoom(z => z - 1)}>-</Button>
    <Button onClick={() => setZoom(z => z + 1)}>+</Button>
    <Button onClick={() => setCoords([43, 13])}>Umbria</Button>
    <Button onClick={() => setCoords([46, 11])}>Trento</Button>

    <Divider />

    <ButtonGroup direction="vertical">
      <Button
        onClick={() => console.log('ciao')}
        icon="💩"
      >CLICK ME</Button>
      <Button
        url="http://www.google.com"
        icon="💩"
      >GOOGLE</Button>
    </ButtonGroup>

    <Card
      title="My Panel"
      icon="fa fa-google"
      type="danger"
      onIconClick={doSomething}
    >
      <MapQuest
        city="Milano"
        zoom={4}
        width="100%"
        className="myborder"
        alt="my map"
        onClick={() => console.log('ciao')}
      />
    </Card>



    <Divider />
    <Divider
      type="dotted"
      color="red"
      height={5}
      margin={2}
    />

    <Card
      title="My Panel"
      icon="fa fa-google"
      type="danger"
      onIconClick={doSomething}
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo magni qui tempora? Debitis facilis nisi omnis reiciendis rem veritatis voluptas voluptates voluptatibus. Aliquam, at cum eum laudantium nemo omnis perferendis?

    </Card>
  </div>
}
