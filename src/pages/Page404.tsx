import { Link } from 'react-router-dom';

export function Page404() {
  return <div>
    Page not found 404
    <hr/>

    <Link to="/">Back to home</Link>
  </div>
}
