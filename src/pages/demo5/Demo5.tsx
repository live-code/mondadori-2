import ErrorMsg from '../../uikit/ErrorMsg';
import CrudList from './components/CrudList';
import { useCrud } from './hooks/useCrud';

export function Demo5() {
  const { error, products, addItem, deleteItem } = useCrud();

  return <div>
    { error && <ErrorMsg>server error</ErrorMsg>}

    <h1>Demo Lista</h1>
    <button onClick={addItem}>ADD</button>

    {products.length === 0 && <div>No products in cart</div>}
    <CrudList products={products} onProductDelete={deleteItem} />
  </div>
}
