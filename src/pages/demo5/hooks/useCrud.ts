import axios from 'axios';
import { useEffect, useState } from 'react';
import { Product } from '../../../model/product';

export function useCrud() {
  const [products, setProducts] = useState<Product[]>([]);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    axios.get<Product[]>('http://localhost:3001/products')
      .then(res => {
        setProducts(res.data)
      })
  }, []);

  function deleteItem(idToRemove: number) {
    setError(false)

    axios.delete(`http://localhost:3001/products/${idToRemove}`)
      .then(() => {
        setProducts(s => s.filter(p => p.id !== idToRemove))
      })
      .catch(() => {
        setError(true)
      })
  }

  function addItem() {
    setError(false)

    axios.post<Product>('http://localhost:3001/products', { title: 'mario' })
      .then(res => {
        setProducts(s => [...s, res.data])
      })
      .catch(() => {
        setError(true)
      })
  }

  return {
    products,
    error,
    addItem,
    deleteItem
  };
}
