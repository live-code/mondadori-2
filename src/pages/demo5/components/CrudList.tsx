import { Product } from '../../../model/product';

interface CrudListProps {
  products: Product[];
  onProductDelete: (id: number) => void;
}

export default function CrudList (props: CrudListProps) {

  return <>
    {
      props.products.map(p => {
        return (
          <li key={p.id}>
            {p.title} {p.id}
            <button onClick={() => props.onProductDelete(p.id)}>delete</button>
          </li>
        )
      })
    }
  </>
};

