import axios from 'axios';
import clsx from 'clsx';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { MyUser } from '../../model/my-user';

export function UserDetails() {
  const [user, setUser] = useState<MyUser | null>(null);
  const [success, setSuccess] = useState(false)
  const params = useParams()

  useEffect(() => {
    axios.get<MyUser>('http://localhost:3001/users/' + params.userId)
      .then(res => {
        setUser(res.data)
      })
  }, []);

  function onSave(e: React.FormEvent<HTMLFormElement>) {
    setSuccess(false)
    e.preventDefault();
    axios.patch<MyUser>('http://localhost:3001/users/' + user?.id, user)
      .then((res) => {
        setSuccess(true)
        setTimeout(() => {
          setSuccess(false)
        }, 2000)
      })
  }

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setUser({ ...user!, [e.currentTarget.name]: e.currentTarget.value })
  }

  console.log(user?.name)

  const isNameValid = user && user.name.length > 3;

  return <div>
    { success && <div className="alert alert-success">
      Ok! Done
      <i className="fa fa-times" onClick={() => setSuccess(false)}></i>
    </div>}
    <h1>{user?.name}</h1>
    {
      user &&
        <form onSubmit={onSave}>
            <input
              className={clsx('form-control', {'is-invalid': !isNameValid, 'is-valid': isNameValid})}
              name="name" type="text" value={user?.name || ''}
                   onChange={onChangeHandler}/>
            <input
              className="form-control"
              name="phone" type="text" value={user?.phone || ''}
                 onChange={onChangeHandler}/>
          <button type="submit" disabled={!isNameValid}>Save</button>
        </form>
    }


  </div>
}
