import axios from 'axios';
import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { MyUser } from '../../model/my-user';

export function Users() {
  const [users, setUsers] = useState<MyUser[]>();

  useEffect(() => {
    axios.get<MyUser[]>('http://localhost:3001/users')
      .then(res => {
        setUsers(res.data)
      })
  }, []);

  return <div>
    { !users && <div>Loading</div> }
    { users?.length === 0 && <div>Non ci sono utenti</div> }
    {
      users?.map(u => {
        return <li key={u.id}>
          {u.name}
          <NavLink to={`/users/${u.id}`}>
            <i className="fa fa-eye" style={{ color: 'black'}}></i>
          </NavLink>
        </li>
      })
    }
  </div>
}
