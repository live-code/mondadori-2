import clsx from 'clsx';
import React from 'react';
import { Product } from '../model/product';
import { User } from '../model/user';


const user: User = {
  id: 1,
  name: 'Fabio',
  gender: 'M',
  description: 'bla bla',
  url: 'http://www.fabiobiondi.dev'
}

const products: Product[] = [
  { id: 1, title: 'Nutella'},
  { id: 2, title: 'Milk'},
]

function Demo1() {
  const renderEmptyMsg = () =>  <div className="alert alert-danger">Nessun prodotto</div>;

  const renderProductMsg = () => (
    <div className="alert alert-success">
      <div>Ci sono {products.length} prodotti nel carrello</div>
    </div>
  )

  function gotoUrl() {
    window.open(user.url)
  }

  return (
    <div>
      <h1>Demo1 </h1>
      <h1>
        {products.length > 0 ? renderProductMsg() : renderEmptyMsg()}
      </h1>

      <div className="card" style={{width: '18rem'}}>
        <div className="card-body">
          <h5 className="card-title">{user.name}</h5>
          <p className="card-text">{user.description}</p>
          <button
            onClick={gotoUrl}
            className={clsx('btn', {
              'male': user.gender === 'M',
              'female': user.gender === 'F',
            } )}
          > Visit WebSite </button>
        </div>
      </div>
    </div>
  );
}

export default Demo1;


/*

document.getElementById('el').addEventListener('click', (event)  => {
  console.log(event)
})*/
