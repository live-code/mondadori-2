import React, { useCallback, useState } from 'react';

const Demo8 = () => {
  const [state, setState] = useState<number>(0)
  const [random, setRandom] = useState<number>(0)

  const inc = useCallback(() => {
    setState(state + 1)
  }, [state])

  return <div>
    <button onClick={() => setRandom(Math.random())}>random: {random}</button>

    Demo 8
    <Parent value={state} onIncrement={inc}/>
  </div>
}
export default Demo8;

// ======================
interface ParentProps {
  value: number;
  onIncrement: () => void;
}
const Parent = React.memo((props: ParentProps) => {
  console.log(' Parent: render', props.value);
  return <div>
    Parent {props.value}
    <Child value={props.value} onIncrement={props.onIncrement} />
  </div>
})
// ======================
interface ChildProps {
  value: number;
  onIncrement: () => void;
}
const Child = React.memo((props: ChildProps) => {
  console.log('  Child: render', props.value);
  return <div>
    Child
    <button onClick={props.onIncrement}>state: {props.value}</button>

  </div>
})
