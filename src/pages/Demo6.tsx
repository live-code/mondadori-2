import { useState } from 'react';

interface Coords {
  x: number;
  y: number;
}

export function Demo6() {
  const [coords, setCoords] = useState<Coords>({ x: 0, y: 0})

  function mouseMoveHandler(e: React.MouseEvent) {
    setCoords({
      x: e.clientX,
      y: e.clientY
    })
  }

  const half = window.innerHeight / 2;
  const tooltipY = coords.y > half ? coords.y - 50 : coords.y

  return <div
    className="main"
    style={ { height: '100vh', backgroundColor: 'lightcyan'} }
    onMouseMove={mouseMoveHandler}
  >  {JSON.stringify(coords)}
    <div className="myTooltip" style={{ left: coords.x - 50, top: tooltipY}}>
      MESSAGGIO
    </div>
  </div>
}
