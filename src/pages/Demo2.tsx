import React, { useState } from 'react';
import { API_AMAZON } from '../config';

export default function Demo2() {
  const [text, setText] = useState<string>('');

  function goto() {
    window.open(`${API_AMAZON}${text}`)
  }

  function changeHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    setText(e.currentTarget.value)
  }

  return <div>
    <h1>Demo2 </h1>
    <input type="text" className="form-control" placeholder="Search on Amazon"
           onInput={changeHandler}/>
    <button
      onClick={goto}
      className="btn btn-primary"
    >SEARCH {text}</button>


  </div>
}

