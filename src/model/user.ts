export interface User {
  id: number;
  name: string;
  gender: 'M' | 'F';
  description: string;
  url: string;
}
