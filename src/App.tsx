import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { NavBar } from './core/NavBar';
import Demo1 from './pages/Demo1';
import { Demo4 } from './pages/Demo4';
import { Demo5 } from './pages/demo5/Demo5';
import { Demo6 } from './pages/Demo6';
import { Demo7 } from './pages/demo7/Demo7';
import { LostPass } from './pages/login/components/LostPass';
import { Registration } from './pages/login/components/Registration';
import { SignIn } from './pages/login/components/SignIn';
import { Login } from './pages/login/Login';
import { Page404 } from './pages/Page404';
import { UserDetails } from './pages/users/UserDetails';
import { Users } from './pages/users/UserList';

const Demo2 = React.lazy(() => import('./pages/Demo2'));
const Demo3 = React.lazy(() => import('./pages/Demo3'));
const Demo8 = React.lazy(() => import('./pages/Demo8'));
const Demo9UiKitDemo = React.lazy(() => import('./pages/Demo9UiKitDemo'));

function App() {

  return (
    <BrowserRouter>
      <NavBar />
      <hr/>
      <Routes>
        <Route
          path="login"
          element={ <Login /> }
        >
          <Route path="signin"  element={ <SignIn /> } />
          <Route path="lostpass" element={ <LostPass /> } />
          <Route path="registration" element={ <Registration /> } />
          <Route index element={
            <Navigate to="signin" />
          } />
        </Route>


        <Route path="demo1" element={ <Demo1 /> } />
        <Route path="demo2" element={
          <React.Suspense fallback={<div>loading...</div>}>
            <Demo2 />
          </React.Suspense>
        } />
        <Route path="demo3" element={
          <React.Suspense fallback={<div>loading...</div>}>
            <Demo3 />
          </React.Suspense>
        } />
        <Route path="demo4" element={ <Demo4 /> } />
        <Route path="demo5" element={ <Demo5 /> } />
        <Route path="demo6" element={ <Demo6 /> } />
        <Route path="demo7" element={ <Demo7 /> } />
        <Route path="demo8" element={
          <React.Suspense fallback={<div>loading...</div>}>
            <Demo8 />
          </React.Suspense>
        } />

        <Route path="demo9" element={
          <React.Suspense fallback={<div>loading...</div>}>
            <Demo9UiKitDemo />
          </React.Suspense>
        } />

        <Route path="users" element={ <Users /> } />
        <Route path="users/:userId" element={ <UserDetails /> } />
        <Route path="404" element={ <Page404 /> } />
        <Route path="/" element={
          <Navigate to="demo1" />
        } />
        <Route path="*" element={
          <Navigate to="404" />
        } />
      </Routes>

    </BrowserRouter>
  );
}

export default App;

