import { PropsWithChildren } from 'react';

export default function ErrorMsg (props: PropsWithChildren) {
  return <div className="alert alert-danger">{props.children}</div>
};

