interface MapQuestProps {
  city: string;
  zoom?: number
}

const API = 'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';

export default function MapQuest (props: MapQuestProps & React.ImgHTMLAttributes<HTMLImageElement>) {
  const { city, zoom = 10, ...rest} = props;

  return <img
    {...rest}
    src={`${API}&center=${city}&size=200,200&zoom=${zoom}`}
    />
};

