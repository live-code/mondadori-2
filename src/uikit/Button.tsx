import { PropsWithChildren } from 'react';

interface ButtonProps {
  icon?: string;
  url?: string;
}

export default function Button (
  props: PropsWithChildren<ButtonProps> & React.ButtonHTMLAttributes<HTMLButtonElement>
) {
  const { icon, children, url, ...rest} = props;

  function onClickHandler(e: React.MouseEvent<HTMLButtonElement>) {
    if (url) {
      window.open(url)
    }
    if (props.onClick) {
      props.onClick(e)
    }
  }

  return <button
    className="btn btn-outline-dark"
    {...rest}
    onClick={onClickHandler}
  >
    {icon}
    {children}
  </button>
};

