import { useEffect } from 'react';

interface RedirectProps {
  to: string;
}

export function Redirect(props: RedirectProps) {

  useEffect(() => {
    window.location.href = props.to;
  }, []);

  return <div>....</div>
}
