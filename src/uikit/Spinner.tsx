export function Spinner(){
  return <div className="progress">
    <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
         style={{ width: '100%'}} />
  </div>
}
