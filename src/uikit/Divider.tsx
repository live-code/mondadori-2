interface DividerProps {
  type?: 'solid' | 'dashed' | 'dotted';
  color?: string;
  height?: 1 | 3 | 5
  margin?: 1 | 2 | 3
}

export function Divider(props: DividerProps) {
  const {
    color = '#222',
    margin = 1,
    height = 3,
    type = 'solid'
  } = props;

  return (
    <div
      style={{
        borderTopWidth: height,
        borderTopStyle: type,
        borderTopColor: color,
        margin: 10 * margin
      }}
    ></div>
  )
}
