import L from "leaflet";
import { useEffect, useRef, useState } from 'react';
import css from './Leaflet.module.css';

interface LeafletProps {
  coords: any;
  zoom: number;
}

export default function Leaflet (props: LeafletProps) {
  const host = useRef<HTMLDivElement | null>(null);
  const map = useRef<L.Map | null>(null);

  useEffect(() => {
    if (host.current && !map.current) {
      map.current = L.map(host.current!).setView(props.coords, props.zoom);
      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(map.current);
    }

    return () => {

    }
  }, []);

  useEffect(() => {
    map.current?.setZoom(props.zoom)
  }, [props.zoom])

  useEffect(() => {
    map.current?.setView(props.coords)
  }, [props.coords])

  return  <div ref={host} className={css.map}></div>
};

