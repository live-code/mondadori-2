import clsx from 'clsx';
import { PropsWithChildren } from 'react';

interface ButtonGroupProps {
  direction?: 'horizontal' | 'vertical'
}

export default function ButtonGroup (props: PropsWithChildren<ButtonGroupProps>) {
  const { direction = 'horizontal'} = props;
  return <div
    className={clsx({
      'btn-group': direction === 'horizontal',
      'btn-group-vertical': direction === 'vertical'
    })}>
    {props.children}
  </div>
};

