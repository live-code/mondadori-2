import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';
import MapQuest from './MapQuest';

export default {
  title: 'Widgets/Maps',
  component: MapQuest,
} as ComponentMeta<typeof MapQuest>;

const Template: ComponentStory<typeof MapQuest> = (args) => <MapQuest {...args}/>;

export const BasicExample = Template.bind({});

BasicExample.args = {
  city: 'Palermo',
  zoom: 4
};

export const WithNoZoom = Template.bind({});

BasicExample.args = {
  city: 'Milano',
};
