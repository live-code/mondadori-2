import clsx from 'clsx';
import { PropsWithChildren, useState } from 'react';

interface CardProps {
  title: string;
  customHeaderCls?: string;
  type?: 'success' | 'danger';
  icon?: string;
  onIconClick?: () => void;
}

export function Card(props: PropsWithChildren<CardProps>) {
  const [isOpen, setIsOpen] = useState<boolean>(true)

  function iconClickHandler(e: React.MouseEvent) {
    e.stopPropagation();
    if (props.onIconClick) {
      props.onIconClick();
    }
  }

  return (
    <div className="card">
      {/*header*/}
      <div
        className={clsx(
          'card-header',
          { 'bg-danger': props.type === 'danger'},
          { 'bg-success': props.type === 'success'},
          props.customHeaderCls,
        )}
      >
        <div className="d-flex justify-content-between align-items-center"
             onClick={() => setIsOpen(!isOpen)}>
          {props.title}
          {props.icon && <div onClick={iconClickHandler}><i className={props.icon}></i></div>}
        </div>
      </div>

      {
        isOpen &&
          <div className="card-body">
            {props.children}
          </div>
      }
    </div>
  )
}
